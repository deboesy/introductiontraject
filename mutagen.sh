#!/usr/bin/env bash
# shellcheck disable=SC2120

if [[ -f ./.docker.env ]]; then source ./.docker.env; fi

LABEL_APP=${LABEL_APP:-docker-app}
LABEL_VENDOR=${LABEL_VENDOR:-docker-vendor}

function pause {
    mutagen sync pause "${LABEL_APP}"
    mutagen sync pause "${LABEL_VENDOR}"
}

function resume {
    MUTAGEN_RESUME=$(mutagen sync list "${LABEL_APP}" 2>/dev/null| grep "Name: ${LABEL_APP}")
    if [[ "${#MUTAGEN_RESUME}" -gt 0 ]]; then
        mutagen sync resume "${LABEL_APP}"
        mutagen sync resume "${LABEL_VENDOR}"
    else
        start
    fi
}

function start {
    mutagen sync create \
               --name="${LABEL_APP}" \
               --default-group-beta=app \
               --default-owner-beta=app \
               --sync-mode=two-way-resolved \
               --default-file-mode=0644 \
               --default-directory-mode=0755 \
               --ignore=/.idea \
               --ignore=/.docker \
               --ignore=/.github \
               --ignore=/vendor \
               --ignore=/node_modules \
               --ignore=*.sql \
               --ignore=*.gz \
               --ignore=*.zip \
               --ignore=*.bz2 \
               --ignore=/.git/**.lock \
               --symlink-mode=posix-raw \
               ./ docker://$(docker-compose ps "${APP_NAME:-application}" |awk  "{if (NR>1) {print \$1}}")/app


    mutagen sync create \
               --name="${LABEL_VENDOR}" \
               --default-group-beta=app \
               --default-owner-beta=app \
               --sync-mode=two-way-resolved \
               --default-file-mode=0644 \
               --default-directory-mode=0755 \
               --symlink-mode=posix-raw \
               ./vendor docker://$(docker-compose ps "${APP_NAME:-application}" |awk  "{if (NR>1) {print \$1}}")/app/vendor

    echo "Waiting for mutagen sync to end"
    sleep 5
    MUTAGEN_UP=$(mutagen sync list | sed -ne "/${LABEL_APP%-*}/,/\--/ p" | grep -E -i -o 'staging|connecting|updating')
    while [[ "${#MUTAGEN_UP}" -gt 0 ]];
    do
        sleep 5
        MUTAGEN_UP=$(mutagen sync list | sed -ne "/${LABEL_APP%-*}/,/\--/ p" | grep -E -o -i 'staging|connecting|updating')
    done
    echo "Mutagen is UP and Watching changes"

}

function destroy {
    mutagen sync terminate "${LABEL_APP}"
    mutagen sync terminate "${LABEL_VENDOR}"
    mutagen daemon stop
}
if [[ "$1" == "destroy" ]]; then
    destroy
elif [[ "$1" == "stop" ]]; then
    pause
else
    resume
fi