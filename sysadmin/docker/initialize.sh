#!/usr/bin/env bash

echo "Starting up docker with kevin"
curl --fail -sS https://bitbucket.org/phpro/phpro-kevin/raw/master/get.sh | bash
kevin pull
kevin up -d

# echo "Installing Composer ..."
# kevin composer install

# echo "Configure grumphp locally"
# ./vendor/bin/grumphp git:init

echo "Application has started!"
