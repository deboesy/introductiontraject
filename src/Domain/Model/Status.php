<?php

namespace App\Domain\Model;

class Status
{
    private string $value;

    function __construct(string $value) {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}