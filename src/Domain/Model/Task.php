<?php

namespace App\Domain\Model;

use DateTimeImmutable;
use phpDocumentor\Reflection\Types\Integer;

final class Task
{
    private int $id;

    private User $assignee;

    private string $title;

    private string $description;

    private Status $status;

    private \DateTimeImmutable $date;

    public static function fromState(array $state): Task
    {
        return new self(
            $state['id'],
            $state['assignee'],
            $state['title'],
            $state['description'],
            $state['status'],
            $state['date']
        );
    }

    function __construct(int $id, User $assignee, string $title, string $description, Status $status, \DateTimeImmutable $date) {
        $this->id = $id;
        $this->assignee = $assignee;
        $this->title = $title;
        $this->description = $description;
        $this->status = $status;
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getAssignee(): User
    {
        return $this->assignee;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return Status
     */
    public function getStatus(): Status

    {
        return $this->status;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

}


