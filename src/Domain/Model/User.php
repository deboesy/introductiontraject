<?php

namespace App\Domain\Model;

use phpDocumentor\Reflection\Types\Integer;

final class User
{
    private int $id;

    private string $username;

    function __construct(int $id, string $username) {
        $this->id = $id;
        $this->username = $username;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string

    {
        return $this->username;
    }

}