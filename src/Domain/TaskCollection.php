<?php

namespace App\Domain;

use App\Domain\Model\Task;

class TaskCollection implements  \IteratorAggregate
{
    protected $tasks = [];

    public function __construct(array $tasks)
    {
        $this->tasks = [ ...$tasks];
    }

    public function getIterator()
    {
        yield from $this->tasks;
    }

}
