<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Mapper;

use App\Domain\Model\Task;
use http\Exception\InvalidArgumentException;

class TaskMapper
{
    public function __construct(private StorageAdapter $adapter)
    {
    }

    public function findById(int $id): Task
    {
        $result = $this->adapter->find($id);

        if ($result === null) {
            throw new InvalidArgumentException("Task #$id not found!");
        }
        return $this->mapRowToTask($result);
    }

    private function mapRowToTask(array $row): Task
    {
        return Task::fromState($row);
    }

    public function toDb(Task $task): array
    {
        return json_decode(json_encode($task), true);
    }
}