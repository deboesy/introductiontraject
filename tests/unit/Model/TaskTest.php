<?php


use PHPUnit\Framework\TestCase;

final class TaskTest extends TestCase
{
    public function testThatWeCanGetTheProperties()
    {
        $user =  new \App\Domain\Model\User(1, 'deboesy');
        $status = new \App\Domain\Model\Status('open');
        $date = new DateTimeImmutable('2022-02-16');
        $task = new \App\Domain\Model\Task(1, $user, 'title', 'description', $status, $date );
        $this->assertSame($task->getId(), 1, "Actual value is not the same as the expected value");
        $this->assertSame($task->getAssignee(), $user, "Actual value is not the same as the expected value");
        $this->assertSame($task->getTitle(), 'title', "Actual value is not the same as the expected value");
        $this->assertSame($task->getDescription(), 'description', "Actual value is not the same as the expected value");
        $this->assertSame($task->getStatus(), $status, "Actual value is not the same as the expected value");
        $this->assertSame($task->getDate(), $date, "Actual value is not the same as the expected value");

    }
}
