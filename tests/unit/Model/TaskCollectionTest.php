<?php


use PHPUnit\Framework\TestCase;

final class TaskCollectionTest extends TestCase
{
    public function testThatWeCanGetTheProperties()
    {
        $user =  new \App\Domain\Model\User(1, 'deboesy');
        $status = new \App\Domain\Model\Status('open');
        $date = new DateTimeImmutable('2022-02-16');
        $task1 = new \App\Domain\Model\Task(1, $user, 'title', 'description', $status, $date);
        $taskCollection = new \App\Domain\TaskCollection([$task1]);
        $this->assertSame(iterator_to_array($taskCollection->getIterator()), [$task1], "Actual value is not the same as the expected value");


    }
}
