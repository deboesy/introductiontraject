<?php


use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testThatWeCanGetTheProperties()
    {
        $user = new \App\Domain\Model\User(1, 'deboesy');
        $this->assertSame($user->getId(), 1, "Actual value is not the same as the expected value");
        $this->assertSame($user->getUsername(), 'deboesy', "Actual value is not the same as the expected value");
    }
}
