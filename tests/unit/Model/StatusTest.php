<?php


use PHPUnit\Framework\TestCase;

final class StatusTest extends TestCase
{
    public function testThatWeCanGetTheValue()
    {
        $status = new \App\Domain\Model\Status('open');
        $actual = "open";
        $this->assertSame($status->getValue(), $actual, "Actual value is not the same as the expected value");
    }
}
